'use strict'


const	Koa = require('Koa'),
		app = new Koa(),
		bodyParser = require('koa-bodyparser');

require('./APIv1/config/databaseInit');  // move to Global/config/

app.use(bodyParser());

require('./Global/config/router')(app);

app.on('error', (err, ctx) => {
  ctx.body = {
  		event: 'error',
	    message: err,
	    data: {}
	  };
}); 




app.listen(3000, () => console.log('Listening on http://localhost:3000'));


