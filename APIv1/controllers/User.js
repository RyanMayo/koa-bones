

const Router = require('koa-router');
// const Demo = require('../models/Demo');
const UserModel = require('../models/User');

module.exports = function(app){

	let UserRoute = new Router({
		prefix: '/v1'
	});


	/**
	* Create user
	*/
	UserRoute.post('/user', async function(ctx, next){

		try {
			
			const userData = await UserModel.create(Object.assign({}, ctx.request.body))

			ctx.status = 201;
			ctx.body = userData;

		} catch (err){
			
			ctx.throw(401, "Sorry, our Bad");
		}

	});


	/**
	 * Read all users
	 */
	UserRoute.get('/users', async function(ctx, next){

		try{
			const userData = await UserModel.findAll({})

			ctx.status = 201;
			ctx.body = userData;
		}

		catch(err){
			ctx.throw(401, "Sorry, our Bad");
		}
		

		
	});


	/**
	 * Read specific user by id
	 */
	UserRoute.get('/user/:id', async function(ctx, next){

		try{
			const userData = await UserModel.find({ where: { id: ctx.params.id } });

			ctx.status = 201;
			ctx.body = userData;
		}
		
		catch (err) {
			ctx.throw(401, "Sorry, Dawg");
		}
		
	});


	/**
	 * Update specific user by id
	 */
	UserRoute.put('/user/:id', async function(ctx, next){
		try{
			const userData = await UserModel.update(
				ctx.request.body,
				{ where: { id: ctx.params.id } }
			);

			ctx.status = 201;
			ctx.body = userData; // outputs true
		}

		catch(err){
			ctx.throw(401);
		}
	});

	/**
	 * Delete specific user by id
	 */

	UserRoute.delete('/user/:id', async function(ctx, next){
		try{
			const userData = await UserModel.update(
				{ active: false},
				{ where: { id: ctx.params.id } }
			);

			ctx.status = 201;
			ctx.body = userData; // returns true
		}

		catch(err){
			ctx.throw(401);
		}
	});


	app.use(UserRoute.routes());
	app.use(UserRoute.allowedMethods());

}