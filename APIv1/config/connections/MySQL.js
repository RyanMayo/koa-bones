const Sequelize = require('sequelize');

const MYSQL = require('../databaseConnections').mysqlAPIv1;
 
module.exports = new Sequelize(
    MYSQL.database,
    MYSQL.user,
    MYSQL.password,
  {
    host: MYSQL.host,
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      idle: 10000,
    },
  },
);