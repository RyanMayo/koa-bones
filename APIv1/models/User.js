'use strict';

const Sequelize = require('sequelize');
const db =require('../config/connections/MySQL');

module.exports =  db.define('user', {
		id: {
			type: Sequelize.UUID,
			primaryKey: true,
			defaultValue: Sequelize.UUIDV4,
			allowNull: false
		},
		name: {
			type: Sequelize.STRING,
			required: true
		},
		password: {
			type: Sequelize.STRING,
			required: true
		},
		role: {
			type: Sequelize.ENUM,
			values: ['user', 'admin', 'disabled']
		},
		active: {
			type: Sequelize.BOOLEAN,
			required: true,
			allowNull: false
		},
		created_at: {
			type: Sequelize.DATE,
			allowNull: false
		},
		updated_at: Sequelize.DATE,
		deleted_at: Sequelize.DATE
	},
	{
		underscored: true
	});

